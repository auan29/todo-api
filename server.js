const express = require('express')
const app = express()

let todos =[
    {
        name : 'Auan',
        id : 1
    },
    {
        name : 'Tanakorn',
        id : 2
    }
]

//SELECT * FROM TODO
app.get('/todos',(req,res) => {
    res.send(todos)
})

app.listen(3000,() =>{
    console.log('TODO API Started at port 3000')
})

//INSERT INTO TODO
app.post('/todos',(req,res) => {
    let newtodos = {
        name : 'Read a book',
        id : 3
    }  


    todos.push(newtodos)
    res.status(201).send()
})